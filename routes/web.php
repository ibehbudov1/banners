<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Banner;

Route::get('/', 'HomeController@front')->name('home');

Route::get('admin', 'HomeController@admin')->name('admin.home');

Route::get('banners', 'HomeController@index')->name('banner.all');

Route::get('banners/{position}', 'HomeController@listBanners')->name('banner.list');

Route::get('banner/delete/{id}', 'HomeController@delete')->name('banner.delete');

Route::post('banner/add', 'HomeController@add')->name('banner.add');

Route::get('banner/redirect/{id}', 'HomeController@redirect')->name('banner.redirect');

Route::get('banner/show/{id}', 'HomeController@show')->name('banner.show');






Route::get('test/{position}', function(\Illuminate\Http\Request $request, $position){


    $banners = request()->session()->get($position.'-banners');

    if($banners)
        foreach ($banners as $banner)
            echo 'id:' . $banner['id'] . ' ' .
                'back counter: ' . $banner['price'] . '<br>';

    else
        echo 'ordering with price';

//
//    $banners = Banner::select('price')->wherePosition('header')->get()->toArray();
//
//    foreach ($banners as $banner)
//        $prices[] = $banner['price'];
//
//
//
////    return 49 % 18;
////    function sum($carry, $item)
////    {
////        $carry += $item;
////        return $carry;
////    }
//
//    function gcd ($a, $b) {
//        return $b ? gcd($b, $a % $b) : $a;
//    }
//
////    echo array_reduce($prices, 'gcd');
//
//
////
////    $banners = Banner::wherePosition('header')->offset(0)->limit(1);
////
////    return $banners->get();


});