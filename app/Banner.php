<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = ['name', 'price', 'position', 'path', 'target'];

    public function statistic()
    {
        return $this->hasMany(Statistic::class);
    }
}
