<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    protected $fillable = ['ip', 'unique_user', 'browser', 'views', 'hits', 'banner_id'];
}
