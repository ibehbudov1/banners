<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Statistic;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public $positions = ['header', 'footer', 'left', 'right'];

    public $statistic;

    public $sourcePath  =   'images/';

    public function __construct()
    {
        $this->statistic = new Statistic();
    }

    public function index()
    {
        $data = [
            'banners'   =>  Banner::all(),
            'positions' =>  $this->positions,
        ];

        return view('all', $data);
    }

    public function front(Request $request)
    {
        $data['banners'] = [];

        $user = [
            'ip'            =>  $request->ip(),
            'browser'       =>  $request->userAgent(),
            'unique_user'   =>  md5($request->ip() . $request->userAgent()),
        ];

        $views = $request->session()->get('count') ?: 1;

        $bannerViews = $views - 1;

        foreach ($this->positions as $position)
        {
            $check = Banner::wherePosition($position);

            if(!$check->first())
                continue;

            if( $bannerViews < $check->count() )
                $banners = Banner::wherePosition($position)->orderBy('price', 'DESC')->offset($bannerViews)->limit(1)->first();

            else
            {
                $banner = $this->getBanner($position);

                $banners = Banner::find($banner['id']);
            }

            if($banners)
                $data['banners'][$position] = $banners;
        }

        $request->session()->put(['count' => $views+1]);

        return view('front', $data);
    }

    public function getBanner($position)
    {
        $banners = $this->getSessionBanners($position);

        $randKey = array_rand($banners);

        $currentBanner = $banners[$randKey];

        $banners[$randKey]['price'] -= 1;

        if($banners[$randKey]['price'] < 1)
            unset($banners[$randKey]);

        $this->updateSessionBanners($banners, $position);

        return $currentBanner;
    }

    public function updateSessionBanners($banners, $position)
    {
        request()->session()->put([$position . '-banners' => $banners]);
    }


    public function admin()
    {
        return view('admin');
    }

    public function listBanners($position)
    {
        if(!in_array($position, $this->positions))
            return redirect(route('admin.home'));

        $data = [
            'banners'   =>  Banner::wherePosition($position)->get(),
            'position'  =>  $position
        ];

        return view('list', $data);
    }

    public function delete(int $id)
    {
        $banner = Banner::find($id);

        if($banner)
        {
            @unlink(public_path('images/' . $banner->path));

            $banner->delete();
        }

        return redirect()->back();
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'name'      =>  'required',
            'price'     =>  'required|numeric',
            'source'    =>  'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'target'    =>  'required'
        ]);

        $image = $request->file('source');

        $path = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path($this->sourcePath);

        $image->move($destinationPath, $path);

        $input = $request->only('name', 'price', 'target');

        $input['path']      = $path;
        $input['position']  = $request->position;

        $banner = Banner::create($input);

        return back()->with('success','Successful');
    }

    public function show(Request $request, int $id)
    {
        $user = [
            'ip'            =>  $request->ip(),
            'browser'       =>  $request->userAgent(),
            'unique_user'   =>  md5($request->ip() . $request->userAgent()),
            'banner_id'     =>  $id
        ];

        $statistics = $this->statistic->where([
            ['banner_id', $id],
            ['unique_user', $user['unique_user']],
        ]);

        if($statistics->first())
        {
            $statistics->increment('views');
        }
        else
            $this->statistic->create($user);


        $banner = Banner::find($id);

        if($banner)
            if(file_exists(public_path($this->sourcePath . $banner->path)))
                return response()->file(public_path($this->sourcePath . $banner->path));
    }

    public function redirect(Request $request, int $id)
    {
        $banner = Banner::find($id);

        if($banner)
        {
            $user = [
                'ip'            =>  $request->ip(),
                'browser'       =>  $request->userAgent(),
                'unique_user'   =>  md5($request->ip() . $request->userAgent()),
                'banner_id'     =>  $id
            ];

            $statistics = $this->statistic->where([
                ['banner_id', $id],
                ['unique_user', $user['unique_user']],
            ]);

            if($statistics->first())
            {
                $statistics->increment('hits');
            }
            else
                $this->statistic->create($user);

            return redirect()->to($banner->target);
        }

        return redirect()->back();
    }

    public function gcd ($a, $b) {
        return $b ? $this->gcd($b, $a % $b) : $a;
    }

    public function getSessionBanners($position)
    {
        if(request()->session()->has($position . '-banners'))
        {
            $banners = request()->session()->get($position . '-banners');

            if(!count($banners) > 0)
            {
                request()->session()->forget($position . '-banners');

                return $this->getSessionBanners($position);
            }
        }
        else
        {
            $banners = Banner::select('id', 'price')->wherePosition($position)->get()->toArray();

            foreach ($banners as $banner)
                $prices[] = $banner['price'];

            $divider = array_reduce($prices, [$this, 'gcd']);

            foreach ($banners as $key => $banner)
                $banners[$key]['price'] /= $divider;


            request()->session()->put([$position . '-banners' => $banners]);
        }

        return $banners;
    }
}



