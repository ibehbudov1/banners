-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.26-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for banners
CREATE DATABASE IF NOT EXISTS `banners` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `banners`;

-- Dumping structure for table banners.banners
CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` enum('header','footer','left','right') COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `banners_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table banners.banners: ~4 rows (approximately)
DELETE FROM `banners`;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` (`id`, `name`, `position`, `path`, `price`, `target`, `created_at`, `updated_at`) VALUES
	(1, 'Header Banner 1', 'header', '1509655976.png', 100, 'https://behbudov.info', '2017-11-02 20:52:56', '2017-11-02 20:52:56'),
	(2, 'Left Banner 1', 'left', '1509655991.jpg', 12, '', '2017-11-02 20:53:11', '2017-11-02 20:53:11'),
	(3, 'Right Banner 1', 'right', '1509656002.png', 33, '', '2017-11-02 20:53:22', '2017-11-02 20:53:22'),
	(4, 'Footer Banner 1', 'footer', '1509656017.png', 12, '', '2017-11-02 20:53:37', '2017-11-02 20:53:37'),
	(5, 'Header Banner 2', 'header', '1509793211.jpg', 50, 'https://behbudov.info2', '2017-11-04 11:00:11', '2017-11-04 11:00:11'),
	(6, 'Header Banner 3', 'header', '1509793211.jpg', 20, 'https://behbudov.info3', '2017-11-04 11:00:11', '2017-11-04 11:00:11'),
	(7, 'Header Banner 4', 'header', '1509793211.jpg', 10, 'https://behbudov.info4', '2017-11-04 11:00:11', '2017-11-04 11:00:11');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
