<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_user');
            $table->ipAddress('ip');
            $table->text('browser');
            $table->integer('banner_id');
            $table->integer('views')->default(1);
            $table->integer('hits')->default(0);
            $table->timestamps();

            $table->unique(['unique_user', 'banner_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistics');
    }
}
