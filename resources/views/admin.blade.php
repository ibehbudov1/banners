@extends('layout')

@section('main')
    <div class="admin header-banner"><a href="{{ route('banner.list', 'header') }}"><h1 class="text-center-2">+</h1></a></div>
    <div class="admin left-banner"><a href="{{ route('banner.list', 'left') }}"><h1 class="text-center-2">+</h1></a></div>
    <div class="admin right-banner"><a href="{{ route('banner.list', 'right') }}"><h1 class="text-center-2">+</h1></a></div>
    <div class="admin footer-banner"><a href="{{ route('banner.list', 'footer') }}"><h1 class="text-center-2">+</h1></a></div>


    <div class="container main-style">
        <h1>Content</h1>
    </div>
@endsection