@extends('layout')

@section('main')

    <div class="container">
        <h1>All banners</h1>
        <table class="table table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>id</th>
                <th>Name</th>
                <th>Target</th>
                <th>Source</th>
                <th>Position</th>
                <th>Price</th>
                <th>Views</th>
                <th>Hits</th>
            </tr>
            </thead>
            <tbody>
            @foreach($banners as $banner)
                <tr>
                    <td>{{ $banner->id }}</td>
                    <td>{{ $banner->name }}</td>
                    <td><a href="{{ $banner->target }}" target="_blank">{{ $banner->target }}</a></td>
                    <td><a href="{{ asset('images/' . $banner->path) }}" target="_blank">{{ $banner->path }}</a></td>
                    <td><a href="{{ route('banner.list', $banner->position) }}">{{ $banner->position }}</a></td>
                    <td>{{ $banner->price }}</td>
                    <td>{{ $banner->statistic()->sum('views') }}</td>
                    <td>{{ $banner->statistic()->sum('hits') }}</td>
                    <td><a onclick="return confirm('are you sure?');" href="{{ route('banner.delete', $banner->id) }}">x</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="container row">
            <div class="col-xs-6">
                @include('errors')
                <form action="{{ route('banner.add') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name">
                    </div>

                    <div class="form-group">
                        <label for="target">Target</label>
                        <input type="text" class="form-control" name="target">
                    </div>

                    <div class="form-group">
                        <label for="price">Price</label>
                        <input type="number" class="form-control" name="price">
                    </div>

                    <div class="form-group">
                        <label for="position">Position</label>
                        <select name="position" class="form-control">
                            @foreach($positions as $position)
                                <option value="{{ $position }}">{{ ucfirst($position) }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="source">Source</label>
                        <input type="file" name="source" class="form-control">
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Submit">
                    </div>
                </form>
            </div>
        </div>

        <a href="{{route('admin.home')}}">&#8592; Back</a>
    </div>

@endsection
