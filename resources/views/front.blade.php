@extends('layout')

@section('main')

    @foreach($banners as $name => $banner)
        <div class="{{ $name }}-banner"><a target="_blank" href="{{ route('banner.redirect', $banner->id) }}"><img src="{{ route('banner.show', [$banner->id, 'time' => time()])}}" alt=""></a></div>
    @endforeach

    <div class="container main-style">
        <h1>Content</h1>
    </div>
@endsection
